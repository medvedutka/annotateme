/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.model.message;

import java.util.LinkedList;
import java.util.List;
import jc.annotateme.model.Annotation;



public class ListAnnotation extends Message {
    private String documentId;
    private Integer page;
    private List<Annotation> annotations;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }
    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }
    public void addAnnotation(Annotation a) {
        if(annotations == null) {
            annotations = new LinkedList<Annotation>();
        }
        annotations.add(a);
    }
    
}
