/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.model;

import java.util.LinkedList;
import java.util.List;



public class Document {
    private String id;
    private String dateCreation;

    private String title;
    private String originalFileName;
    
    private List<Annotation> annotations;// = new LinkedList<Annotation>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }
    
    public void addAnnotation(Annotation annotation) {
        if(getAnnotations() == null) {
            setAnnotations( new LinkedList<Annotation>());
        }
        this.annotations.add(annotation);
    }
    
}
