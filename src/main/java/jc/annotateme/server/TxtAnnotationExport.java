/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import jc.annotateme.model.Annotation;
import jc.annotateme.model.Document;



public class TxtAnnotationExport {
    
    
    public String export(Document document) {
         StringBuilder builder = new StringBuilder();
         
         if(document.getAnnotations() != null) {
            ArrayList<Annotation> annotations = new ArrayList<Annotation>(document.getAnnotations());
            Collections.sort(annotations, new Comparator<Annotation>() {
                @Override
                public int compare(Annotation o1, Annotation o2) {
                    if(o1.getPage() == null) {
                        return -1;
                    }
                    if(o2.getPage() == null) {
                        return 1;
                    }
                    return o1.getPage() - o2.getPage();
                }
            });
            
            Object currentPage = new Object();
            Object previousPage = new Object();
            for(Annotation annotaation : annotations) {
                currentPage = annotaation.getPage();
                if(! IsEqual(currentPage,previousPage)) {
                    String pageNbStr = (annotaation.getPage() == null) ? "#" : ""+(annotaation.getPage()+1);
                    builder.append("Page ").append(pageNbStr).append(":\n");
                }
                builder.append("\t* ").append(annotaation.getText());
                builder.append("\n");
                previousPage = currentPage;
            }
         }
         
         return builder.toString();
    }
    
    private static boolean IsEqual(Object o1, Object o2) {
        if(o1 == o2) {
            return true;
        }
        if(o1 == null || o2 == null) {
            return false;
        }
        return o1.equals(o2);
    }
}
