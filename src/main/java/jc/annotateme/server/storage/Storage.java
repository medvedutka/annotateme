/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server.storage;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.ta.TransparentActivationSupport;
import com.db4o.ta.TransparentPersistenceSupport;
import java.io.File;
import java.util.List;
import jc.annotateme.model.Annotation;
import jc.annotateme.server.Config;

/**
 *
 * @author thecat
 */
public class Storage {
    public Storage() {
        
    }
    
    
    static final String DB_FILENAME = "annotateme.db";
    private void getDBAccess() {
        File dbFile = new File(Config.Get().getDBPath(),DB_FILENAME);
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().add(new TransparentActivationSupport());    
        config.common().add(new TransparentPersistenceSupport());
        ObjectContainer db = Db4oEmbedded.openFile(config, dbFile.getAbsolutePath());
        try {
            // do something with db4o
        } finally {
            db.close();
        }
    }
    
    public List<Annotation> retrieveAnnotations(String docId, int page) {
        return null;
    }
    
    public void deleteAnnotation(String docId,int page, String annotationId) {
        
    }
}
