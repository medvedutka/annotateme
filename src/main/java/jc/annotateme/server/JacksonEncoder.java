/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import jc.annotateme.model.message.Message;
import org.atmosphere.config.managed.Encoder;

import java.io.IOException;

/**
* Encode a {@link Message} into a String
*/
public class JacksonEncoder implements Encoder<Message, String> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String encode(Message m) {
        try {
            return mapper.writeValueAsString(m);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}