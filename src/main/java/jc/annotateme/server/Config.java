/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;



public class Config {
    
    private static final Logger LOGGER = java.util.logging.Logger.getLogger("AnnotateMeConfig");
    
    private static Config config;
    public static Config Get() {
        if(config == null) {
            config = new Config();
        }
        return config;
    }
    
    
    private PropertiesConfiguration configurationFile;
    private Config() {
        
        configurationFile = new PropertiesConfiguration();
        String confFile = System.getProperty("annotateme.configfile");
        if(confFile != null && !confFile.isEmpty() && new File(confFile).isFile()) {
            try {
                configurationFile.setFile(new File(confFile));
                FileChangedReloadingStrategy reloadingStrategy = new FileChangedReloadingStrategy();
                reloadingStrategy.setRefreshDelay(10000);
                configurationFile.setReloadingStrategy(reloadingStrategy);
                configurationFile.load();
            }
            catch(Exception e) {
                LOGGER.log(Level.SEVERE, "Cannot load specified configuration file: " + confFile , e);
            }
        }
        else {
            configurationFile = new PropertiesConfiguration();
        }
        configurationFile.setThrowExceptionOnMissing(false);
        
    }
    
    
    private static final String dataPathFilePropertyKey = "_dataPath_File"; 
    public File getDataPaph() {
        File configDataPathFile = (File)configurationFile.getProperty(dataPathFilePropertyKey);
        if(configDataPathFile == null) {
            String configDataPath = configurationFile.getString("dataPath");
            if(configDataPath == null) {
                try {
                    configDataPathFile = java.nio.file.Files.createTempDirectory("AnnotateMe").toFile();
                    configurationFile.setProperty(dataPathFilePropertyKey, configDataPathFile);
                }
                catch(Exception e) {
                    LOGGER.log(Level.SEVERE, "Cannot create temp folder for data storage", e);
                }
            }
            else {
                configDataPathFile = new File(configDataPath);
                configurationFile.setProperty(dataPathFilePropertyKey, configDataPathFile);
            }
            configDataPathFile.mkdirs();
            LOGGER.log(Level.INFO, "Using data directory: " + configDataPathFile.getAbsolutePath());
        }
        return configDataPathFile;
    }
    
    public File getDBPath() {
        File f = new File(getDataPaph(),"db");
        f.mkdirs();
        return f;
    }
    
    public File getDocumentPath() {
        File f = new File(getDataPaph(),"doc");
        f.mkdirs();
        return f;
    }

    public File getTmpPath() {
        File f = new File(getDataPaph(),"tmp");
        f.mkdirs();
        return f;
    }
    
    String pdfLibrary = "JPedalRenderer"; //Default
    public String getPDFLibrary() {
        return pdfLibrary; 
    }
}
