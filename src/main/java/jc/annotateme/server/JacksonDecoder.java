/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import jc.annotateme.model.message.Message;
import org.atmosphere.config.managed.Decoder;


public class JacksonDecoder implements Decoder<String, Message> {

    public JacksonDecoder() {
        mapper = new ObjectMapper();
    }

    private final ObjectMapper mapper;

    @Override
    public Message decode(String s) {
        try {
            Message msg = mapper.readValue(s, Message.class);
            return msg;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
