/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jc.annotateme.server;

import com.db4o.EmbeddedObjectContainer;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import jc.annotateme.model.message.Message;
import java.io.IOException;
import java.util.Iterator;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import jc.annotateme.model.Annotation;
import jc.annotateme.model.Document;
import jc.annotateme.model.message.CreateAnnotation;
import jc.annotateme.model.message.DeleteAnnotation;
import jc.annotateme.model.message.ListAnnotation;
import jc.annotateme.model.message.NewAnnotation;
import jc.annotateme.model.message.UpdateAnnotation;
import jc.annotateme.server.storage.Db4oServletListener;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Ready;
import org.atmosphere.config.service.Resume;
import static org.atmosphere.cpr.ApplicationConfig.MAX_INACTIVE;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.MetaBroadcaster;
import org.atmosphere.util.IOUtils;

//@Config
@ManagedService(path = "/AnnotateMeHandler",atmosphereConfig = MAX_INACTIVE + "=120000")
public class AnnotateMeAtmosphereService {

    private static final Logger logger = java.util.logging.Logger.getLogger("AnnotateMeAtmosphereService");

    // Uncomment for changing response's state
    /*@Get
    public void init(AtmosphereResource r) {
        r.getResponse().setCharacterEncoding("UTF-8");
    }*/

    /**
     * Invoked when the connection as been fully established and suspended, e.g
     * ready for receiving messages.  
     * @param r
     */
    @Ready
    public void onReady(final AtmosphereResource r) {
        logger.log(Level.INFO, "Browser {} connected." + r.uuid());
    }

    /**
     * Invoked when the client disconnect or when an unexpected closing of the
     * underlying connection happens.
     *     
     * @param event
     */
    @Disconnect
    public void onDisconnect(AtmosphereResourceEvent event) {
        if (event.isCancelled()) {
            logger.log(Level.INFO, "Browser {} unexpectedly disconnected", event.getResource().uuid());
        } else if (event.isClosedByClient()) {
            logger.log(Level.INFO, "Browser {} closed the connection", event.getResource().uuid());
        }
    }
    
    @Resume
    public void onResume(AtmosphereResourceEvent r) {
    }

    
    private final JacksonEncoder messageToJSON = new JacksonEncoder();
    private final JacksonDecoder jsonToMessage = new JacksonDecoder();
    
    @org.atmosphere.config.service.Post
    public void onPost(AtmosphereResource r) {
        StringBuilder b = IOUtils.readEntirely(r);
        if (b.length() > 0) {
            try {
                Message msg = jsonToMessage.decode(b.toString());
                if(msg == null) {
                    throw new Exception("Cannot decode (jsonToMessage) posted message");
                }
                Message result = onMessage(r,msg);
                //r.write(messageToJSON.encode(result));
                r.getBroadcaster().broadcast(messageToJSON.encode(result), r);
                //r.write(messageToJSON.encode(getOkMessage()));
            }
            catch(Exception e) {
                logger.log(Level.SEVERE, "Cannot handle posted message", e);
            }
        } else {
            logger.warning("{} received an empty body");
        }
    }
    
    
//,broadcaster = ExcludeSessionBroadcaster.class
    //@org.atmosphere.config.service.Message(/*encoders = {JacksonEncoder.class}, */decoders = {JacksonDecoder.class})
    public Message onMessage(AtmosphereResource r,Message message) throws IOException {
        logger.log(Level.INFO, "OnMessage from resourcse: "+r.uuid()+">");

        EmbeddedObjectContainer dbServer = Db4oServletListener.DB_SERVER;//(EmbeddedObjectContainer) r.getRequest().getRequest().getServletContext().getAttribute(Db4oServletListener.KEY_DB4O_SERVER);
        ObjectContainer dbSession = dbServer.ext().openSession();        

        try {
            if(message instanceof ListAnnotation) {
                ListAnnotation lam = (ListAnnotation)message;
                Broadcaster b =  BroadcasterFactory.getDefault().lookup("/" + lam.getDocumentId() +"/" + r.uuid(),true);
                b.addAtmosphereResource(r);
                return onMessageListAnnotation(dbSession, r,lam);
            }
            /*else if(message instanceof CreateAnnotation) {
                return onMessageCreateAnnotation(dbSession, r,(CreateAnnotation)message);
            }
            else if(message instanceof DeleteAnnotation) {
                onMessageDeleteAnnotation(dbSession, r,(DeleteAnnotation)message);
                return getOkMessage();
            }
            else if(message instanceof UpdateAnnotation) {
                onMessageUpdateAnnotation(dbSession, r,(UpdateAnnotation)message);
                return getOkMessage();
            }*/
        }
        catch(Exception e) {
            logger.log(Level.SEVERE, "Error proceding message", e);
            return getBadMessage("Error: " +e.getMessage());
        }
        finally {
            if(dbSession != null) {
                dbSession.close();
            }
        }
        
        return getBadMessage("Message not understood");
    }
    
    
    private Message okMessage = null;
    private Message getOkMessage() {
        if(okMessage == null) {
            Message msg = new Message();
            msg.setAuthor("server");
            msg.setClientUUID("server");
            msg.setTime(System.currentTimeMillis());
            msg.setMessage("ok");
            okMessage = msg;
        }
        return okMessage;
    }

    private Message getBadMessage(String msgText) {
        Message msg = new Message();
        msg.setAuthor("server");
        msg.setClientUUID("server");
        msg.setTime(System.currentTimeMillis());
        msg.setMessage(msgText);
        return msg;
    }
    
    private Document loadDocument(ObjectContainer dbSession, String documentId) throws Exception {
        Document document = new Document();
        document.setId(documentId);
        ObjectSet<Document> documents  = dbSession.queryByExample(document);
        if(documents.size() != 1) {
            throw new Exception("No document found with ID: " + documentId);
        }
        return documents.next();
    }
    private Annotation loadAnnotation(ObjectContainer dbSession, String annotationId) throws Exception {
        Annotation annotation = new Annotation();
        annotation.setId(annotationId);
        ObjectSet<Annotation> annotations  = dbSession.queryByExample(annotation);
        if(annotations.size() != 1) {
            throw new Exception("No annotation found with ID: " + annotationId);
        }
        return annotations.next();
    }
    
    
    
    
    private ListAnnotation onMessageListAnnotation(ObjectContainer dbSession,AtmosphereResource r,ListAnnotation lam) throws Exception {
        Document document = loadDocument(dbSession,lam.getDocumentId());
        
        dbSession.ext().refresh(document, Integer.MAX_VALUE);
        if(document.getAnnotations() != null) {
            for(Annotation a : document.getAnnotations()) {
                if(Objects.equals(a.getPage(),lam.getPage())) {
                    lam.addAnnotation(a);
                }
            }
        }
        
        return lam;
    }
    
    
    /*
    private CreateAnnotation onMessageCreateAnnotation(ObjectContainer dbSession,AtmosphereResource r,CreateAnnotation ca) throws Exception {
        Annotation annot = new Annotation();
        annot.setId(UUID.randomUUID().toString());
        annot.setText(ca.getText());
        annot.setX(ca.getX());
        annot.setY(ca.getY());
        annot.setWidth(ca.getW());
        annot.setHeight(ca.getH());
        if(ca.getDocumentPage() != null) {
            annot.setPage(ca.getDocumentPage());
        }
        
        Document document = loadDocument(dbSession,ca.getDocumentId());
        
        synchronized(DB_LOCK) {
            dbSession.ext().refresh(document, Integer.MAX_VALUE);
            dbSession.store(annot);
            document.addAnnotation(annot);
            dbSession.store(document);
            dbSession.store(document.getAnnotations());
            dbSession.commit();
        }
        
        ca.setResult(annot);
                
        NewAnnotation newAnnotationMessage = new NewAnnotation();
        newAnnotationMessage.setDocumentId(document.getId());
        newAnnotationMessage.setAnnotation(annot);
        
        //r.getBroadcaster().broadcast(messageToJSON.encode(newAnnotationMessage));
        MetaBroadcaster.getDefault().broadcastTo("/" + ca.getDocumentId() + "/*",messageToJSON.encode(newAnnotationMessage));
        
        return ca;
    }
    
    private void onMessageUpdateAnnotation(ObjectContainer dbSession,AtmosphereResource r,UpdateAnnotation ua) throws Exception {
        Annotation annot = loadAnnotation(dbSession,ua.getAnnotationId());
        synchronized(DB_LOCK) {
            dbSession.ext().refresh(annot, Integer.MAX_VALUE);
            if(ua.getX() != null) {
                annot.setX(ua.getX());
            }
            if(ua.getY() != null) {
                annot.setY(ua.getY());
            }
            if(ua.getW() != null) {
                annot.setWidth(ua.getW());
            }
            if(ua.getH() != null) {
                annot.setHeight(ua.getH());
            }
            if(ua.getText() != null) {
                annot.setText(ua.getText());
            }
            dbSession.store(annot);
            dbSession.commit();
        }
        
        //r.getBroadcaster().broadcast(messageToJSON.encode(ua));
        
        MetaBroadcaster.getDefault().broadcastTo("/" + ua.getDocumentId() + "/*",messageToJSON.encode(ua));
    }
    
    
     
    private void onMessageDeleteAnnotation(ObjectContainer dbSession,AtmosphereResource r,DeleteAnnotation da) throws Exception {
        Document document = loadDocument(dbSession,da.getDocumentId());
        synchronized(DB_LOCK) {
            dbSession.ext().refresh(document, Integer.MAX_VALUE);
            Annotation annotationToDelete = null;
            if(document.getAnnotations() != null) {
                Iterator<Annotation> iter = document.getAnnotations().iterator();
                while(iter.hasNext()) {
                    Annotation a = iter.next();
                    if(a.getId().equals(da.getAnnotationId())) {
                        annotationToDelete = a;
                        iter.remove();
                        break;
                    }
                }
                
                if(annotationToDelete != null) {
                    dbSession.delete(annotationToDelete);
                }
            }
            
            dbSession.store(document);
            dbSession.commit();
        }
        
        //r.getBroadcaster().broadcast(messageToJSON.encode(da));
        
        MetaBroadcaster.getDefault().broadcastTo("/" + da.getDocumentId() + "/*",messageToJSON.encode(da));
    }
    */
}
