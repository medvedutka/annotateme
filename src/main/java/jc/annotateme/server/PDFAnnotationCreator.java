package jc.annotateme.server;

import java.io.File;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import jc.annotateme.model.Annotation;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.color.PDGamma;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationSquareCircle;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDBorderStyleDictionary;


public class PDFAnnotationCreator {
    float inch = 72;
    PDGamma colourRed = new PDGamma();
    PDGamma colourBlue = new PDGamma();
    PDGamma colourBlack = new PDGamma();
    PDBorderStyleDictionary borderThick = new PDBorderStyleDictionary();
    PDBorderStyleDictionary borderThin = new PDBorderStyleDictionary();
    PDBorderStyleDictionary borderULine = new PDBorderStyleDictionary();

    PDDocument document;
    
    
    public static void AddAnnotationsTo(File pdfFile, List<Annotation> annotations, OutputStream result) throws Exception {
        PDFAnnotationCreator pdfAnnotationCreator = new PDFAnnotationCreator();
        try {
            pdfAnnotationCreator.load(pdfFile);
            pdfAnnotationCreator.addAnnotations(annotations);
            pdfAnnotationCreator.save(result);
        }
        catch(Exception e) {
            throw e;
        }
        finally {
            pdfAnnotationCreator.close();
        }
    }
    
    public PDFAnnotationCreator() {
        colourRed.setR(1);
        colourBlue.setB(1);
        borderThick.setWidth(inch/12);  // 12th inch
        borderThin.setWidth(inch/72); // 1 point
        borderULine.setStyle(PDBorderStyleDictionary.STYLE_UNDERLINE);
        borderULine.setWidth(inch/72); // 1 point
    }
    
    public void load(File pdfFile) throws Exception {
        document = PDDocument.load(pdfFile);
    }

  
    public void addAnnotations(List<Annotation> annotations) throws Exception
    {
        List<PDPage> listPages = document.getDocumentCatalog().getAllPages();
        
        for(Annotation annotation : annotations) {
            PDPage page = listPages.get(annotation.getPage());
            List pageAnnotations = page.getAnnotations();

            PDRectangle pageSize = page.findCropBox(); // page.findMediaBox() ???
            float minX = pageSize.getLowerLeftX();
            float minY = pageSize.getLowerLeftY();
            float maxX = pageSize.getUpperRightX();
            float maxY = pageSize.getUpperRightY();
            float pW = pageSize.getWidth();
            float pH = pageSize.getHeight();

            // a square annotation
            PDAnnotationSquareCircle aSquare = new PDAnnotationSquareCircle( PDAnnotationSquareCircle.SUB_TYPE_SQUARE);
            aSquare.setContents(annotation.getText());
            //aSquare.setTitlePopup("googoogo");
            aSquare.setColour(colourBlue); 
            aSquare.setBorderStyle(borderThick);

            
            float pdfAnnotLowerLeftX = minX + (annotation.getX() * pW);
            float pdfAnnotLowerLeftY = minY + ((1.f - annotation.getY()) * pH);
            float pdfAnnotUpperRightX = minX + (annotation.getX() * pW) + (annotation.getWidth() * pW);
            float pdfAnnotUpperRightY = minY + ((1.f - annotation.getY()) * pH) - (annotation.getHeight() * pH) ;
            
            // Place the annotation on the page, we'll make this 1" (72points) square 3.5" down, 1" in from the right on the page
            PDRectangle position = new PDRectangle(); // Reuse the variable, but note it's a new object!
            position.setLowerLeftX(pdfAnnotLowerLeftX);  // 1" in from right, 1" wide
            position.setLowerLeftY(pdfAnnotLowerLeftY); // 1" height, 3.5" down
            position.setUpperRightX(pdfAnnotUpperRightX); // 1" in from right
            position.setUpperRightY(pdfAnnotUpperRightY); // 3.5" down
            aSquare.setRectangle(position);

            //  add to the annotations on the page
            pageAnnotations.add(aSquare);
        }
    }
    
    public void save(OutputStream outputStream) throws Exception {
        document.save(outputStream);
    }
    
    public void close() {
        try {
            if(document != null) {
                document.close();
            }
        }
        catch(Exception e) {
            java.util.logging.Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage());
        }
    }
}
