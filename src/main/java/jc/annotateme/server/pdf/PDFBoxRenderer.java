/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server.pdf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

/**
 *
 * @author thecat
 */
public class PDFBoxRenderer implements PDFLibrary {
    
    @Override
    public int getNumberOfPage(File pdfFile) throws Exception {
        PDDocument document = PDDocument.load(pdfFile);
        return document.getDocumentCatalog().getAllPages().size();
    }
    
    @Override
    public BufferedImage renderAsImage(File pdfFile,int pageNumber) throws Exception {
        PDDocument document = PDDocument.load(pdfFile);
        List<PDPage> list = document.getDocumentCatalog().getAllPages();

        PDPage page = list.get(pageNumber);
        BufferedImage image = page.convertToImage();

        document.close();
        return image;
    }
}
