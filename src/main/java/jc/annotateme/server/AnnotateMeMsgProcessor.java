/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jc.annotateme.server;

import com.db4o.EmbeddedObjectContainer;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.IOException;
import java.util.Iterator;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import jc.annotateme.model.Annotation;
import jc.annotateme.model.Document;
import jc.annotateme.model.message.CreateAnnotation;
import jc.annotateme.model.message.DeleteAnnotation;
import jc.annotateme.model.message.ListAnnotation;
import jc.annotateme.model.message.Message;
import jc.annotateme.model.message.NewAnnotation;
import jc.annotateme.model.message.UpdateAnnotation;
import jc.annotateme.server.storage.Db4oServletListener;



public class AnnotateMeMsgProcessor {
    
    private static final Logger LOGGER = java.util.logging.Logger.getLogger("AnnotateMeManager");
    
    private static final Object DB_LOCK = new Object();

    
    private AnnotateMeChannelService channelService;
    public AnnotateMeMsgProcessor(AnnotateMeChannelService _channelService) {
        this.channelService = _channelService;
    }
    
    
    private void broadcast(String documentId, Message message) {
        channelService.broadcast(documentId, message);
    }
    
    
    public Message process(Message message) throws IOException {

        EmbeddedObjectContainer dbServer = Db4oServletListener.DB_SERVER;//(EmbeddedObjectContainer) r.getRequest().getRequest().getServletContext().getAttribute(Db4oServletListener.KEY_DB4O_SERVER);
        ObjectContainer dbSession = dbServer.ext().openSession();        

        try {
            if(message instanceof ListAnnotation) {
                ListAnnotation lam = (ListAnnotation)message;
                return onMessageListAnnotation(dbSession,lam);
            }
            else if(message instanceof CreateAnnotation) {
                return onMessageCreateAnnotation(dbSession, (CreateAnnotation)message);
            }
            else if(message instanceof DeleteAnnotation) {
                onMessageDeleteAnnotation(dbSession, (DeleteAnnotation)message);
                return getOkMessage();
            }
            else if(message instanceof UpdateAnnotation) {
                onMessageUpdateAnnotation(dbSession, (UpdateAnnotation)message);
                return getOkMessage();
            }
        }
        catch(Exception e) {
            LOGGER.log(Level.SEVERE, "Error proceding message", e);
            return getBadMessage("Error: " +e.getMessage());
        }
        finally {
            if(dbSession != null) {
                dbSession.close();
            }
        }
        
        return getBadMessage("Message not understood");
    }
    
    
    private Message okMessage = null;
    private Message getOkMessage() {
        if(okMessage == null) {
            Message msg = new Message();
            msg.setAuthor("server");
            msg.setClientUUID("server");
            msg.setTime(System.currentTimeMillis());
            msg.setMessage("ok");
            okMessage = msg;
        }
        return okMessage;
    }

    private Message getBadMessage(String msgText) {
        Message msg = new Message();
        msg.setAuthor("server");
        msg.setClientUUID("server");
        msg.setTime(System.currentTimeMillis());
        msg.setMessage(msgText);
        return msg;
    }
    
    private Document loadDocument(ObjectContainer dbSession, String documentId) throws Exception {
        Document document = new Document();
        document.setId(documentId);
        ObjectSet<Document> documents  = dbSession.queryByExample(document);
        if(documents.size() != 1) {
            throw new Exception("No document found with ID: " + documentId);
        }
        return documents.next();
    }
    private Annotation loadAnnotation(ObjectContainer dbSession, String annotationId) throws Exception {
        Annotation annotation = new Annotation();
        annotation.setId(annotationId);
        ObjectSet<Annotation> annotations  = dbSession.queryByExample(annotation);
        if(annotations.size() != 1) {
            throw new Exception("No annotation found with ID: " + annotationId);
        }
        return annotations.next();
    }
    
    
    
    
    private ListAnnotation onMessageListAnnotation(ObjectContainer dbSession, ListAnnotation lam) throws Exception {
        Document document = loadDocument(dbSession,lam.getDocumentId());
        
        dbSession.ext().refresh(document, Integer.MAX_VALUE);
        if(document.getAnnotations() != null) {
            for(Annotation a : document.getAnnotations()) {
                if(Objects.equals(a.getPage(),lam.getPage())) {
                    lam.addAnnotation(a);
                }
            }
        }
        
        return lam;
    }
    
    
    
    private CreateAnnotation onMessageCreateAnnotation(ObjectContainer dbSession,CreateAnnotation ca) throws Exception {
        Annotation annot = new Annotation();
        annot.setId(UUID.randomUUID().toString());
        annot.setText(ca.getText());
        annot.setX(ca.getX());
        annot.setY(ca.getY());
        annot.setWidth(ca.getW());
        annot.setHeight(ca.getH());
        if(ca.getDocumentPage() != null) {
            annot.setPage(ca.getDocumentPage());
        }
        
        Document document = loadDocument(dbSession,ca.getDocumentId());
        
        synchronized(DB_LOCK) {
            dbSession.ext().refresh(document, Integer.MAX_VALUE);
            dbSession.store(annot);
            document.addAnnotation(annot);
            dbSession.store(document);
            dbSession.store(document.getAnnotations());
            dbSession.commit();
        }
        
        ca.setResult(annot);
                
        NewAnnotation newAnnotationMessage = new NewAnnotation();
        newAnnotationMessage.setDocumentId(document.getId());
        newAnnotationMessage.setAnnotation(annot);

        broadcast(document.getId(), newAnnotationMessage);
        
        return ca;
    }
    
    private void onMessageUpdateAnnotation(ObjectContainer dbSession,UpdateAnnotation ua) throws Exception {
        Annotation annot = loadAnnotation(dbSession,ua.getAnnotationId());
        synchronized(DB_LOCK) {
            dbSession.ext().refresh(annot, Integer.MAX_VALUE);
            if(ua.getX() != null) {
                annot.setX(ua.getX());
            }
            if(ua.getY() != null) {
                annot.setY(ua.getY());
            }
            if(ua.getW() != null) {
                annot.setWidth(ua.getW());
            }
            if(ua.getH() != null) {
                annot.setHeight(ua.getH());
            }
            if(ua.getText() != null) {
                annot.setText(ua.getText());
            }
            dbSession.store(annot);
            dbSession.commit();
        }
        
        broadcast(ua.getDocumentId(),ua);
    }
    
    
     
    private void onMessageDeleteAnnotation(ObjectContainer dbSession,DeleteAnnotation da) throws Exception {
        Document document = loadDocument(dbSession,da.getDocumentId());
        synchronized(DB_LOCK) {
            dbSession.ext().refresh(document, Integer.MAX_VALUE);
            Annotation annotationToDelete = null;
            if(document.getAnnotations() != null) {
                Iterator<Annotation> iter = document.getAnnotations().iterator();
                while(iter.hasNext()) {
                    Annotation a = iter.next();
                    if(a.getId().equals(da.getAnnotationId())) {
                        annotationToDelete = a;
                        iter.remove();
                        break;
                    }
                }
                
                if(annotationToDelete != null) {
                    dbSession.delete(annotationToDelete);
                }
            }
            
            dbSession.store(document);
            dbSession.commit();
        }
        
        broadcast(da.getDocumentId(),da);
    }
    
    
}
